# Google firestore ORM (unofficial)

A simple ORM to be used with Google Cloud Firestorm. This project is heavily inspired by MongoEngine, an engine to interact with a mongo database.
To start using this similarly outside of a google environment, you may need to call the following methods:

```python
# Configures the firestore client
google_firestore_orm.configure(**kwargs)

# If you want to have blob support (through cloud storage)
google_firestore_orm.configure_storage(**kwargs)
```

Then, you can define models like so:

```python
from google_firestore_orm import Document

class MyModel(Document):
    a_field = StringField(unique=True)

# Access the query manager:
MyModel.objects.filter(a_field='ola')
```

Note that this project is made with a simple mindset, no caching and such is applied within this project. So please be
careful. While a lot of validation is applied, this may not be foolproof.
