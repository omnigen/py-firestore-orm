from setuptools import setup, find_packages

setup(name='google-firestore-orm',
      version='0.1',
      description='A simple ORM for Firestore',
      url='https://bitbucket.org/omnigen/py-firestore-orm',
      author='Wesley Ameling',
      author_email='w.ameling@omnigen.nl',
      license='MIT',
      packages=find_packages(),
      install_requires=[
          'google-cloud-firestore',
          'google-cloud-storage'
      ],
      classifiers=[
          'Programming Language :: Python',
          'Programming Language :: Python :: 3.7',
      ],
      zip_safe=False)
