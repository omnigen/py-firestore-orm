from .query_set import QuerySet


class MethodCallQuerySet(QuerySet):

    def __init__(self, super_args, method, *args, **kwargs):
        super().__init__(*super_args)
        self._method = method
        self._args = args
        self._kwargs = kwargs

    def _get_query(self):
        query = super()._get_query()
        return getattr(query, self._method)(*self._args, **self._kwargs)
