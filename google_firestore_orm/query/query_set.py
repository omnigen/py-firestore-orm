from google.cloud.firestore_v1 import DocumentReference

from google_firestore_orm import get_configured_client
from google_firestore_orm._utils import find_document_fields, safe_next
from google_firestore_orm.exceptions import InvalidFieldException

_CLASS_CACHE = dict()


class QuerySet(object):

    def __init__(self, document_class, query):
        self._document_class = document_class
        self._base_query = query

    @classmethod
    def get_manager(cls, document_class):
        """
        The recommended method to create a QuerySet object, as this will
        cache the object and will apply some sanity checking which is not
        applied in the original constructor
        """
        from ..document import Document

        document_class_name = document_class.__name__
        if document_class in _CLASS_CACHE:
            return _CLASS_CACHE[document_class_name]

        assert issubclass(document_class, Document)
        if not hasattr(document_class, '_fields'):
            fields = find_document_fields(document_class)
            setattr(document_class, '_fields', fields)
        _CLASS_CACHE[document_class_name] = QuerySet(
            document_class, document_class.get_collection())
        # Apply a default ordering
        meta = getattr(document_class, 'meta', dict())
        ordering = meta.get('ordering', list())
        if ordering:
            _CLASS_CACHE[document_class_name] = (
                _CLASS_CACHE[document_class_name].order_by(*ordering))

        return _CLASS_CACHE[document_class_name]

    def _validate_fields(self, *fields):
        for field in fields:
            if field not in self._document_class._fields:
                raise InvalidFieldException(field)

    def _get_query(self):
        return self._base_query

    def _execute_query(self):
        return self._get_query().stream()

    def _get_init_params(self):
        return self._document_class, self._get_query()

    def _to_document(self, document_snapshot):
        if document_snapshot is not None:
            return self._document_class(_doc_snapshot=document_snapshot)

    def count(self):
        # The cursor doesn't know how much documents are available
        # But we want this method to be as efficient as possible
        # Therefore, we select no fields so that the fewest amount of data
        # is transferred
        query = self._get_query().select(tuple())
        count = 0
        for _ in query.stream():
            count += 1
        return count

    def distinct(self, field_name):
        self._validate_fields(field_name)

        results = set()
        query = self._get_query().select((field_name,))
        field = self._document_class._fields[field_name]
        for document_snapshot in query.stream():
            raw_value = document_snapshot.get(field_name)
            cloned_field = field.initialize_field(
                field_name, self._document_class.get_collection(),
                value=raw_value)
            if isinstance(cloned_field.value, list):
                results.update(cloned_field.value)
            else:
                results.add(cloned_field.value)
        return list(results)

    def only(self, *fields):
        from .method_call_query_set import MethodCallQuerySet

        self._validate_fields(*fields)
        return MethodCallQuerySet(
            self._get_init_params(), 'select', fields)

    def limit(self, limit):
        from .method_call_query_set import MethodCallQuerySet
        return MethodCallQuerySet(
            self._get_init_params(), 'limit', limit)

    def skip(self, skip):
        from .method_call_query_set import MethodCallQuerySet
        return MethodCallQuerySet(
            self._get_init_params(), 'offset', skip)

    def first(self):
        try:
            return self.limit(1).get()
        except self._document_class.DoesNotExist:
            return None

    def all(self):
        return list(self)

    def order_by(self, *fields):
        from .order_by_query_set import OrderByQuerySet

        query_set = self
        for field in fields:
            query_set = OrderByQuerySet(query_set._get_init_params(), field)
        return query_set

    def update(self, **kwargs):
        # Check if all keys exist
        for key in kwargs:
            self._validate_fields(key)

        for doc_snapshot in self._get_query().select(list()).stream():
            # Validators of each have to be run each time a document is
            # updated, otherwise for example the unique validator won't work
            mapped_kwargs = dict()
            for key, value in kwargs.items():
                field = self._document_class._fields[key]
                cloned_field = field.initialize_field(
                    field.name, field.collection)
                cloned_field.set_value(value)
                cloned_field.validate()
                mapped_kwargs[key] = cloned_field.get_db_value()
            doc_snapshot.update(**mapped_kwargs)

    def clone(self):
        """
        No need for cloning, as the #stream method just takes the information from
        the query class and yields the results in a single method
        """
        return self

    def filter(self, **filters):
        from .filtered_query_set import FilteredQuerySet
        return FilteredQuerySet(self._get_init_params(), **filters)

    def get(self, identifier=None):
        if identifier is None:
            # execute the query
            obj = safe_next(self._execute_query())
            if not obj:
                raise self._document_class.DoesNotExist()
            return self._to_document(obj)
        else:
            # retrieve by id
            client = get_configured_client()
            collection = self._document_class.get_collection()
            reference = DocumentReference(
                collection.id, identifier, client=client)
            _doc_snapshot = reference.get()
            if not _doc_snapshot.exists:
                raise self._document_class.DoesNotExist(
                    self._document_class.get_collection_name(), identifier)
            return self._document_class(_doc_snapshot=_doc_snapshot)

    def delete(self):
        for document in self:
            document.delete()

    def __iter__(self):
        return map(self._to_document, self._execute_query())

    def __call__(self, **filters):
        return self.filter(**filters)
