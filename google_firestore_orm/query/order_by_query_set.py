from .query_set import QuerySet


class OrderByQuerySet(QuerySet):

    def __init__(self, super_args, field):
        super().__init__(*super_args)
        self._mode = '+'
        if field.startswith('-') or field.startswith('+'):
            self._mode = field[0]
            field = field[1:]
        self._validate_fields(field)
        self._field = field

    def _get_query(self):
        direction = 'ASCENDING' if self._mode == '+' else 'DESCENDING'
        return super()._get_query().order_by(
            self._field, direction=direction)
