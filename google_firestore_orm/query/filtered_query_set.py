from google_firestore_orm.exceptions import InvalidFilterException
from .query_set import QuerySet

_OPERATOR_MAP = {
    'contains': 'array_contains',
    'lt': '<',
    'lte': '<=',
    'gt': '>',
    'gte': '>='
}


class Or(object):

    def __init__(self, *values):
        self.values = values


def _convert_value_to_db(field_name, document_class, value):
    cloned_field = document_class._fields[field_name].initialize_field(
        field_name, document_class.get_collection())
    cloned_field.set_value(value)
    return cloned_field.get_db_value()


class _OrQuerySet(object):

    def __init__(self, base_query, field_name, operator, converted_values,
                 document_class):
        self._base_query = base_query
        self._document_class = document_class
        if isinstance(base_query, _OrQuerySet):
            self.queries = []
            for sub_query in base_query.queries:
                self.queries.extend(
                    _OrQuerySet.get_queries(
                        sub_query, field_name, operator, converted_values))
        else:
            self.queries = _OrQuerySet.get_queries(
                base_query, field_name, operator, converted_values)

    def where(self, field, operator, value):
        return FilteredQuerySet.factory(
            self, field, operator, self._document_class, value)

    @staticmethod
    def get_queries(base_query, field_name, operator, converted_values):
        return [
            base_query.where(field_name, operator, value)
            for value in converted_values
        ]


class FilteredQuerySet(QuerySet):

    def __init__(self, super_args, **filters):
        super().__init__(*super_args)
        self._filters = filters

    def _get_query(self):
        query = super()._get_query()
        for q_filter, value in self._filters.items():
            split_filter = q_filter.split('__')
            if len(split_filter) == 1:
                field_name, query_operator = split_filter[0], None
            elif len(split_filter) == 2:
                field_name, query_operator = split_filter
                if query_operator not in _OPERATOR_MAP:
                    raise InvalidFilterException(q_filter)
            else:
                raise InvalidFilterException(q_filter)

            fields = self._document_class._fields
            if field_name not in fields:
                raise InvalidFilterException(q_filter, invalid_field=True)

            operator = '=='
            if query_operator is not None:
                supported_query_ops = fields[field_name].supported_query_ops
                if query_operator not in supported_query_ops:
                    raise InvalidFilterException(
                        q_filter, additional_msg='Unsupported operator.')
                operator = _OPERATOR_MAP[query_operator]
            # Convert the value to the db value
            query = FilteredQuerySet.factory(
                query, field_name, operator, self._document_class, value)
        return query

    @staticmethod
    def factory(query, field_name, operator, document_class, value):
        if isinstance(value, Or):
            return _OrQuerySet(query, field_name, operator, list(map(
                lambda v: _convert_value_to_db(field_name, document_class, v),
                value.values
            )), document_class)
        else:
            value = _convert_value_to_db(field_name, document_class, value)
            return query.where(field_name, operator, value)
