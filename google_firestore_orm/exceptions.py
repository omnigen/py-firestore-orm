class GoogleFirestoreOrmException(Exception):
    message = None

    def __init__(self, message=None):
        if message is None:
            message = self.message
        super().__init__(message)


class ClientNotConfiguredException(GoogleFirestoreOrmException):
    """
    Exception for when the firestore client is not configured
    """
    message = ('Firestore client not configured! Please use'
               ' google_firestore_orm.configure!')


class EmptyDocumentException(GoogleFirestoreOrmException):
    """
    Exception for when no fields are found for a model
    """

    def __init__(self, class_name):
        super().__init__(f'No fields found on {class_name}')


class NonExistentFieldException(GoogleFirestoreOrmException):
    """
    Exception for when a field is attempted to be set which does not exist
    """

    def __init__(self, field):
        super().__init__(f'The field {field} does not exist!')


class UniqueNotSupportedException(GoogleFirestoreOrmException):
    """
    Exception when a subfield or custom datastructure does not support
    unique modifiers
    """
    message = 'Unique not supported on this field'


class ReferenceNotFoundException(GoogleFirestoreOrmException):
    """
    Exception when a reference cannot be found
    """

    def __init__(self, collection_name, identifier):
        super().__init__(f'Reference {identifier} for "{collection_name}" could '
                         f'not be found')


class UnsavedReferenceException(GoogleFirestoreOrmException):
    """
    A referenced document has not been saved yet
    """

    def __init__(self, collection_name):
        super().__init__(f'A referenced document from {collection_name} has '
                         f'not been saved yet')


class ReferenceException(GoogleFirestoreOrmException):
    """
    A referenced document is deleted but not removed in the reference field
    """

    def __init__(self, collection):
        super().__init__(f'The document is still referenced in the collection'
                         f' {collection}')


class ValidationException(GoogleFirestoreOrmException):
    """
    Exception for when a value of a field is not valid
    """
    pass


class RequiredFieldException(ValidationException):
    """
    Exception for when a field is required
    """

    def __init__(self, field):
        super().__init__(f'Required field {field} not set')


class NotUniqueException(ValidationException):
    """
    Exception for when a field is not unique
    """

    def __init__(self, field, value):
        super().__init__(f'Value {value} is not unique for field {field}')


class RangeException(ValidationException):
    """
    Exception for when a field is not within a specified range
    """

    def __init__(self, field, value):
        message = f'Field {field} should be '
        if field.min_value is not None:
            message += f'>= {field.min_value}'
        if field.min_value is not None and field.max_value is not None:
            message += ' and '
        if field.max_value is not None:
            message += f'<= {field.max_value}'
        super().__init__(message)


class UndefinedChoiceException(ValidationException):
    """
    Exception for when a field is not one of the defined choices
    """

    def __init__(self, field, choices):
        sorted_choices = list(choices)
        sorted_choices.sort()
        super().__init__(f'Field {field} must be one of'
                         f' {",".join(sorted_choices)}')


class QueryException(GoogleFirestoreOrmException):
    """
    Base exception for query related things
    """
    pass


class InvalidFilterException(QueryException):
    """
    Exception for when a filter on a query is invalid
    """

    def __init__(self, filter_key, invalid_field=False, additional_msg=None):
        base_message = f'Filter {filter_key} is invalid'
        if invalid_field:
            base_message += '. Invalid field name'
        if not additional_msg:
            additional_msg = '.'
        else:
            additional_msg = '. ' + additional_msg
        super().__init__(base_message + additional_msg)


class InvalidFieldException(QueryException):
    """
    Exception for when an invalid field is used during querying
    """

    def __init__(self, field):
        super().__init__(f'Invalid field: {field}')


class ObjectDoesNotExistException(QueryException):
    """
    Exception for when a .get() doesn't return any objects
    """
    pass


class BlobNotFoundException(GoogleFirestoreOrmException):
    """
    Exception for when a file is not found
    """

    def __init__(self, file_name):
        super().__init__(f'File not found {file_name}')
