import re

from google_firestore_orm.exceptions import ObjectDoesNotExistException

BANNED_FIELD_NAMES = ['id', '_doc_snapshot', 'meta', 'objects']


def find_document_fields(cls):
    """
    Finds the fields defined on a document

    :param cls: The document class to look for field attributes
    :type cls: class
z
    :return: A dictionary of field, where the keys are the attribute names and\
        the value the field instances.
    :rtype: dict
    """
    from google_firestore_orm.document.fields import BaseField

    field_dict = dict()
    for attr_name in dir(cls):
        if attr_name == 'objects':
            continue
        attr = getattr(cls, attr_name)
        if isinstance(attr, BaseField):
            field_dict[attr_name] = attr
            if attr_name in BANNED_FIELD_NAMES:
                raise ValueError(f'Banned field name {attr}')
            if '__' in attr_name:
                raise ValueError(
                    f'__ not allowed in field names (for: {attr})')
            delattr(cls, attr_name)
    return field_dict


def class_to_collection_name(class_name):
    """
    Converts a class name to a collection name

    :param class_name: The class name to convert
    :type class_name: str

    :return: The collection name converted from the class name
    :rtype: str
    """
    class_name = class_name.strip()
    if class_name[0].isupper():
        class_name = class_name[0].lower() + class_name[1:]
    return re.sub('[A-Z]', lambda m: f'_{m.group(0).lower()}', class_name)


def safe_next(iter):
    """
    Attempts to get the next value of the iterable. When a
    StopIteration is observed, nothing will be returned.

    :param iter: The iterable
    :type iter: iter

    :return: The next value of the iterable
    :rtype: object
    """
    try:
        return next(iter)
    except StopIteration:
        return


def raise_exc(exception):
    """
    Function which can be used to raise exceptions in lambdas

    :param exception: The exception to raise
    :type exception: Exception
    """
    raise exception


def add_reference(clazz, reference_class):
    """
    Adds a reference to a class, used for the reference field

    :param clazz: The class to add the reference to
    :type clazz: cls
    :param reference_class: The class to add as reference
    :type reference_class: cls
    """
    if not hasattr(clazz, '_references'):
        clazz._references = set()
    clazz._references.add(reference_class)


def get_reference_query(reference_class, field, field_name, doc_class,
                        identifier):
    """
    Checks whether the field is a reference field, or ListField containing
    a reference field, and creates a query to check for a referenced
    object.

    :param reference_class: The class from where the field originates from
    :type reference_class: cls
    :param field: The field to check
    :type field: google_firestore_orm.fields.BaseField
    :param field_name: The name of the field to check
    :type field_name: str
    :param doc_class: The class which should be referenced to
    :type doc_class: cls
    :param identifier: The identifier of the object to create the query for
    :type identifier: str

    :return: A query object when the field can contain a reference of \
        document_class.
    :rtype: google.cloud.firestore_v1.query.Query(
    """
    from google_firestore_orm.document import ReferenceField, ListField

    if isinstance(field, ReferenceField) and field.document_class == doc_class:
        collection = reference_class.get_collection()
        return collection.where(field_name, '==', identifier)
    elif (isinstance(field, ListField)
          and isinstance(field.sub_field, ReferenceField)
          and field.sub_field.document_class == doc_class):
        collection = reference_class.get_collection()
        return collection.where(field_name, 'array_contains', identifier)


def bind_does_not_exist(cls):
    """
    Binds a DoesNotExist attribute to the class which represents an exception

    :param cls: The class to attach the exception to
    :type cls: cls
    """
    cls.DoesNotExist = type(
        'DoesNotExist', (ObjectDoesNotExistException,), {
            '__module__': cls.__module__,
            '__qualname__': f'{cls.__qualname__}.DoesNotExist'
        })
