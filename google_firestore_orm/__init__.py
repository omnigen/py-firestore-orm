from google.auth.exceptions import DefaultCredentialsError
from google.cloud import firestore_v1 as firestore
from google.cloud import storage


_configured_datastore_client = None
_configured_storage_client = None


def configure(**kwargs):
    """
    Configures the firestore client to use.

    :param kwargs: The kwargs passed to the firestore client. Please refer to \
        https://googleapis.github.io/google-cloud-python/latest/firestore/client.html
    """
    global _configured_datastore_client

    _configured_datastore_client = firestore.Client(**kwargs)


def get_configured_client():
    """
    Retrieves the configured client when available, otherwise raises a
    ClientNotConfiguredException.

    :return: The configured firestore client
    :rtype: google.cloud.firestore_v1.Client
    :raises ClientNotConfiguredException: When the client is not configured
    """
    from google_firestore_orm.exceptions import ClientNotConfiguredException

    if _configured_datastore_client is None:
        # Try to automatically configure
        try:
            configure()
        except DefaultCredentialsError:
            raise ClientNotConfiguredException()
    return _configured_datastore_client


def configure_storage(**kwargs):
    """
    Configures the storage client to use.

    :param kwargs: The kwargs passed to the storage client. Please refer to \
        https://googleapis.github.io/google-cloud-python/latest/storage/client.html
    """
    global _configured_storage_client

    _configured_storage_client = storage.Client(**kwargs)


def get_configured_storage_client():
    """
    Retrieves the configured client when available, otherwise raises a
    ClientNotConfiguredException.

    :return: The configured storage client
    :rtype: google.cloud.storage.Client
    :raises ClientNotConfiguredException: When the client is not configured
    """
    from google_firestore_orm.exceptions import ClientNotConfiguredException

    if _configured_storage_client is None:
        # Try to automatically configure
        try:
            configure_storage()
        except DefaultCredentialsError:
            raise ClientNotConfiguredException()
    return _configured_storage_client
