import os
import uuid
from io import BytesIO

from google_firestore_orm import get_configured_storage_client
from google_firestore_orm.exceptions import BlobNotFoundException


class _WritableBlob(BytesIO):

    def __init__(self, set_buffer):
        """
        :param bucket: The bucket to download the file from
        :type bucket: google.cloud.storage.bucket.Bucket

        :param name: The name of the file
        :type name: str
        """
        super().__init__()
        self._set_buffer = set_buffer

    def close(self):
        self._set_buffer(self)


class FileProxy(object):

    def __init__(self, file_name=None):
        self._bucket_name = None
        self._bucket = None
        self._uuid = uuid
        self._folder_prefix = None
        self._buffer = None
        self._written = False
        self.filename = file_name

    def is_populated(self):
        return self._bucket_name is not None

    def populate(self, bucket_name=None, folder_prefix=None, uuid=None):
        self._bucket_name = bucket_name
        self._folder_prefix = folder_prefix
        if uuid is not None:
            self._uuid = uuid

    def _set_buffer(self, buffer):
        self._buffer = buffer
        self._written = True

    @property
    def uuid(self):
        assert self.is_populated()
        if self._uuid is None:
            self._uuid = uuid.uuid4()
        return self._uuid

    @property
    def bucket(self):
        assert self.is_populated()
        if self._bucket is None:
            client = get_configured_storage_client()
            self._bucket = client.bucket(self._bucket_name)
            if not self._bucket.exists():
                self._bucket.create()
        return self._bucket

    def _internal_file_name(self):
        if not self.filename:
            raise ValueError('file name not set')
        file_name = f'{self.filename}-{self.uuid}'
        if self._folder_prefix:
            file_name = os.path.join(self._folder_prefix, file_name)
        return file_name

    def open(self, mode):
        if len(mode) != 2:
            raise ValueError('Only supported modes are rb and wb!')
        first_binary = mode[0] == 'b'
        second_binary = mode[1] == 'b'
        if not first_binary and not second_binary:
            raise ValueError('Only supported modes are rb and wb!')
        second_char = mode[1] if first_binary else mode[0]
        if second_char not in 'rw':
            raise ValueError('Only supported modes are rb and wb!')

        if second_char == 'w':
            return _WritableBlob(self._set_buffer)
        else:
            # r mode
            if not self._buffer:
                self._buffer = BytesIO()
                blob = self.bucket.get_blob(self._internal_file_name())
                if not blob:
                    raise BlobNotFoundException(self.filename)
                blob.download_to_file(self._buffer)
            self._buffer.seek(0)
            return self._buffer

    def delete(self):
        if not self.filename:
            return
        blob = self.bucket.get_blob(self._internal_file_name())
        if blob:
            blob.delete()

    def save(self):
        if self._written and self._buffer:
            blob = self.bucket.blob(self._internal_file_name())
            blob.upload_from_file(self._buffer, rewind=True)
            self._written = False

    @classmethod
    def unserialize_name(cls, name):
        components = name.split(':')
        if len(components) != 4:
            raise ValueError('Invalid serialization')
        proxy = cls(file_name=components[2])
        bucket_name = components[0]
        folder_prefix = components[1] or None
        uuid = components[2]
        proxy.populate(
            bucket_name=bucket_name, folder_prefix=folder_prefix, uuid=uuid)
        return proxy

    def serialize_name(self):
        if not self.filename:
            return None
        return (f'{self._bucket_name}:{self._folder_prefix}:'
                f'{self.filename}:{self.uuid}')
