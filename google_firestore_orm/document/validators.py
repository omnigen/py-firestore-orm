from google_firestore_orm.exceptions import (
    RequiredFieldException, NotUniqueException, RangeException,
    UndefinedChoiceException)

from .._utils import safe_next


class Validator(object):
    """
    Validates a value of a field. The validators are prioritized, meaning that
    the validators available are first sorted based on the priority. The lower
    the priority, the earlier the validator is called.
    """
    priority = 0

    def __init__(self, field):
        """
        The field to validate

        :param field: The field to validate (used to get options from)
        :type field: google_firestore_orm.document.fields.BaseField
        """
        self.field = field

    def validate(self, value):
        """
        Validates a value according to the settings of the bound field

        :param value: The value to validate
        :type value: object

        :raises google_firestore_orm.exceptions.ValidationException: When the \
            value is invalid according to this validator
        """
        pass


class RequiredValidator(Validator):
    """
    Validates whether the value of the field was set or not
    """

    def validate(self, value):
        if not self.field.value_set:
            raise RequiredFieldException(self.field)


class UniqueValidator(Validator):
    """
    Validates whether the value is unique in the collection. This is executed
    with a high priority (one of the last validators) as the lookup can be
    seen expensive. It is advantageous to first run other validators as those
    may fail earlier.
    """
    priority = 10

    def validate(self, value):
        if not self.field.unique:
            return

        documents = self.field.collection.where(
            self.field.name, '==', self.field.get_db_value()
        ).limit(1).stream()

        if safe_next(documents) is not None:
            raise NotUniqueException(self.field, value)


class RangeValidator(Validator):
    """
    Validates whether a field is within a specified range
    """

    def validate(self, value):
        if self.field.min_value is not None and value < self.field.min_value:
            raise RangeException(self.field, value)
        if self.field.max_value is not None and value > self.field.max_value:
            raise RangeException(self.field, value)


class ChoicesValidator(Validator):
    """
    Validates whether a field is one of the defined choices
    """

    def validate(self, value):
        if self.field.choices is not None and value not in self.field.choices:
            raise UndefinedChoiceException(self.field, self.field.choices)
