from datetime import datetime

from google.cloud.firestore_v1 import DocumentReference

from google_firestore_orm import get_configured_client
from google_firestore_orm.document.file_proxy import FileProxy
from google_firestore_orm.exceptions import (
    UniqueNotSupportedException, ReferenceNotFoundException,
    UnsavedReferenceException)
from . import _structs
from .validators import (
    RequiredValidator, UniqueValidator, RangeValidator, ChoicesValidator)

NOT_PROVIDED = object()


class BaseField(object):
    field_validators = [RequiredValidator, UniqueValidator]
    supported_query_ops = []

    def __new__(cls, *args, **kwargs):
        instance = super().__new__(cls)
        instance.__params = (args, kwargs)
        return instance

    def __init__(self, required=True, default=None, unique=False):
        self.name = None
        self.collection = None

        self.required = required
        self.default = default
        self.unique = unique

        self.value = None
        self.value_set = False
        self._loaded_from_db = False
        self._bound = False

        self.validators = []
        if hasattr(self.__class__, 'field_validators'):
            sorted_field_validators = sorted(
                self.__class__.field_validators,
                key=lambda validator: validator.priority)
            for field_validator in sorted_field_validators:
                if callable(field_validator):
                    field_validator = field_validator(self)
                self.validators.append(field_validator)

    def initialize_field(self, field_name, collection, value=NOT_PROVIDED):
        """
        Clones the configuration of this field and makes this field live so
        that it can be used on different instances.

        :param field_name: The name of this field
        :type field_name: str
        :param collection: The collection where the document containing this \
            field will be stored.
        :type collection: google.firestore_v1.collection.CollectionReference
        :param value: The value which was retrieved from the database
        :type value: object

        :return: The cloned instance ready for a live document
        :rtype: BaseField
        """
        cloned_instance = self.__class__(*self.__params[0], **self.__params[1])

        cloned_instance.name = field_name
        cloned_instance.collection = collection
        cloned_instance._bound = True
        if value is not NOT_PROVIDED:
            cloned_instance.from_db_value(value)
            cloned_instance._loaded_from_db = True
        return cloned_instance

    def set_value(self, value):
        """
        Sets the value of this field

        :param value: The value to set
        :type value: object
        """
        same_value = self.value == value
        self.value = value
        self.value_set = True
        self._loaded_from_db = self._loaded_from_db and same_value

    def should_save(self):
        """
        Checks whether the value of this field has been should_save or not

        :return: Whether this field has been should_save
        :rtype: bool
        """
        return not self._loaded_from_db

    def mark_saved(self):
        """
        Marks this field as saved in its current state
        """
        self._loaded_from_db = True

    def delete(self):
        """
        Delete the value associated of this field. This is only required to
        implement when data is stored outside of the document object
        """
        pass

    def validate(self):
        """
        Validates the bound value according to attached validators

        :raises google_firestore_orm.exceptions.ValidationException: When the \
            value did not pass validation
        """
        if self.should_save():
            for validator in self.validators:
                validator.validate(self.value)

    def get_db_value(self):
        """
        Transforms the value to a value which is representable in the database.
        This method is called after value validation. Note that some validators
        may use this method to apply some queries in the database, meaning that
        if this transformation is expensive, it is wise to use a cache.

        :return: The value as it should appear in the database
        :rtype: object
        """
        if not self.value_set:
            return self.default() if callable(self.default) else self.default
        return self.value

    def from_db_value(self, value):
        """
        Transforms the database value to the python value, opposite of
        `get_db_value`. Please call this super method with the transformed
        value.

        :param value: The value to transform
        :type value: object
        """
        self.value = value
        self.value_set = True

    def __str__(self):
        return self.name

    def __eq__(self, other):
        if not isinstance(other, BaseField):
            return False
        return self.value == other.value


class ReferenceField(BaseField):
    field_validators = [RequiredValidator]

    def __init__(self, document_class, **kwargs):
        super().__init__(**kwargs)
        self._document_class = document_class

    @property
    def document_class(self):
        from .document import Document,convert_str_to_document_class

        if isinstance(self._document_class, str):
            self._document_class = convert_str_to_document_class(
                self._document_class)
        assert issubclass(self._document_class, Document)
        return self._document_class

    def get_db_value(self):
        if self.value is not None:
            if self.value.id is None:
                raise UnsavedReferenceException(
                    self.document_class.get_collection_name())
            return self.value.id
        return super().get_db_value()

    def from_db_value(self, value):
        return self.document_class.objects.get(value)

    def validate(self):
        super().validate()
        if self.value is not None and not isinstance(
                self.value, self.document_class):
            raise TypeError(f'{self.field.name} must be of type '
                            f'{self.document_class.__name__}, not '
                            f'{type(self.value)}')


class _NativeTypeField(BaseField):
    type_callable = None
    convert_callable = None

    def _check_type(self, value):
        if not isinstance(value, self.type_callable):
            raise ValueError(f'Invalid type: {type(value)}')
        if self.convert_callable is not None:
            return self.convert_callable(value)
        return value

    def __init__(self, default=None, **kwargs):
        if default is not None:
            default = self._check_type(default)
        kwargs['default'] = default
        super().__init__(**kwargs)

    def set_value(self, value):
        value = self._check_type(value)
        super().set_value(value)


class _NumberField(_NativeTypeField):
    field_validators = _NativeTypeField.field_validators + [RangeValidator]
    supported_query_ops = ['lt', 'lte', 'gt', 'gte']

    def __init__(self, min_value=None, max_value=None, **kwargs):
        super().__init__(**kwargs)
        self.min_value = min_value
        self.max_value = max_value

        if min_value is not None and max_value is not None:
            if min_value >= max_value:
                raise ValueError('min_value should be less than max_value')


class IntField(_NumberField):
    type_callable = int


class FloatField(_NumberField):
    type_callable = float


class StringField(_NativeTypeField):
    field_validators = _NativeTypeField.field_validators + [ChoicesValidator]
    type_callable = str

    def __init__(self, choices=None, **kwargs):
        super().__init__(**kwargs)
        self.choices = set(choices) if choices else choices


class BooleanField(_NativeTypeField):
    type_callable = bool


class DateTimeField(_NativeTypeField):
    type_callable = datetime


class _DataStructureProxyField(_NativeTypeField):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.value = self.convert_callable()
        self.value.set_parent_field(self)
        self.value_set = True
        if self.unique:
            raise UniqueNotSupportedException()

    def validate(self):
        self.value.validate()

    def from_db_value(self, value):
        self.value.from_db_value(value)

    def get_db_value(self):
        return self.value.get_db_value()

    def set_value(self, value):
        if value is None:
            value = self.convert_callable()
            value.set_parent_field(self)
        super().set_value(value)

    # Methods to let IDE's think it works
    def __getitem__(self, item):
        pass

    def __setitem__(self, key, value):
        pass


class ListField(_DataStructureProxyField):
    type_callable = list
    convert_callable = _structs.List
    supported_query_ops = ['contains']

    def __init__(self, sub_field, **kwargs):
        super().__init__(**kwargs)
        self.sub_field = sub_field
        if not isinstance(sub_field, BaseField):
            raise ValueError('sub_field is not of instance BaseField')
        if self.sub_field.unique:
            raise UniqueNotSupportedException()

    def mark_saved(self):
        self.value.mark_saved()
        super().mark_saved()

    def delete(self):
        self.value.delete()
        super().delete()


class DictField(_DataStructureProxyField):
    type_callable = dict
    convert_callable = _structs.Dict


class FileField(BaseField):

    def __init__(self, bucket_name='firestore-blob-storage', folder_prefix=None,
                 **kwargs):
        super().__init__(**kwargs)
        if self.unique:
            raise UniqueNotSupportedException()
        self._bucket_name = bucket_name
        self._folder_prefix = folder_prefix
        self.from_db_value(None)
        self.value_set = True

    def open(self, mode):
        return self.value.open(mode)

    def from_db_value(self, value):
        if value is not None:
            self.value = FileProxy.unserialize_name(value)
        else:
            self.value = FileProxy()
            self.value.populate(bucket_name=self._bucket_name,
                                folder_prefix=self._folder_prefix)

    def get_db_value(self):
        return self.value.serialize_name()

    def mark_saved(self):
        self.value.save()
        super().mark_saved()

    def delete(self):
        self.value.delete()
