import sys
from collections import OrderedDict

from google.cloud.firestore_v1 import DocumentSnapshot

from google_firestore_orm import get_configured_client
from google_firestore_orm.document._class_property import classproperty
from google_firestore_orm.exceptions import (
    EmptyDocumentException, NonExistentFieldException, ReferenceException,
    NotUniqueException)
from .._utils import (
    find_document_fields, class_to_collection_name, safe_next,
    get_reference_query, add_reference, bind_does_not_exist)
from ..query import QuerySet

_LOADED_MODULES = set()
_LOADED_DOCUMENTS = dict()


def _init_document_class(cls):
    """
    Eagerly initialises a document class and document classes in the module

    :param cls: The class to load
    :type cls: cls
    """
    assert issubclass(cls, Document)

    if not hasattr(cls, '_fields'):
        from google_firestore_orm.document import ReferenceField, ListField

        # discover fields and bind references
        cls._fields = find_document_fields(cls)
        for field in cls._fields.values():
            if isinstance(field, ReferenceField):
                add_reference(field.document_class, cls)
            elif (isinstance(field, ListField)
                  and isinstance(field.sub_field, ReferenceField)):
                add_reference(field.sub_field.document_class, cls)
        qualified_name = f'{cls.__module__}.{cls.__name__}'
        _LOADED_DOCUMENTS[qualified_name] = cls
        # Attach does not exist exception
        bind_does_not_exist(cls)

    # Check the module for other document classes
    if cls.__module__ not in _LOADED_MODULES:
        _LOADED_MODULES.add(cls.__module__)
        module = sys.modules[cls.__module__]
        for attr_name in dir(module):
            attr = getattr(module, attr_name)
            if isinstance(attr, Document):
                _init_document_class(attr)


def convert_str_to_document_class(class_name):
    if '.' in class_name:
        # fully qualified class name
        # deliberately throw key error when not existent
        return _LOADED_DOCUMENTS[class_name]
    else:
        # class name, check if multiple of these classes are loaded
        available_classes = list(filter(
            lambda qualified_name: qualified_name.endswith(f'.{class_name}'),
            _LOADED_DOCUMENTS.keys()))
        if len(available_classes) == 1:
            return _LOADED_DOCUMENTS[available_classes[0]]
        else:
            raise ValueError(f'Multiple classes loaded with .{class_name}, '
                             f'please use one of {available_classes}')


class _DocumentProxy(object):

    def __init__(self, document_class, fields, collection, doc_ref=None):
        self._document_class = document_class
        self._collection = collection

        self._doc_ref = doc_ref
        self.id = doc_ref.id if doc_ref else None
        # Should be loaded last, otherwise the __setattr__ will
        # fail during init
        self._fields = fields

    def _validate_fields(self):
        for field in self._fields.values():
            if field.should_save():
                field.validate()
        # unique together
        meta = getattr(self._document_class, 'meta', dict())
        unique_together = meta.setdefault('unique_together', list())
        for unique_together_group in unique_together:
            query = self._document_class.get_collection()
            for field in unique_together_group:
                query = query.where(
                    field, '==', self._fields[field].get_db_value())
            if safe_next(query.limit(1).stream()):
                fields = ','.join(unique_together_group)
                values = ','.join(
                    map(str, map(lambda key: self._fields[key].value,
                                 unique_together_group)))
                raise NotUniqueException(fields, values)

    def _get_reference(self):
        if isinstance(self._doc_ref, DocumentSnapshot):
            self._doc_ref = self._doc_ref.reference
        return self._doc_ref

    def _to_update_dict(self, force_all=False):
        field_data = dict()
        for field_name, field in self._fields.items():
            if field.should_save() or force_all:
                field_data[field_name] = field.get_db_value()
        return field_data

    def delete(self):
        reference = self._get_reference()
        if reference:
            for field in self._fields.values():
                field.delete()
            reference.delete()

    def save(self):
        ref = self._get_reference()
        self._validate_fields()
        update_dict = self._to_update_dict()

        if not update_dict and ref:
            return
        # Create or update
        if not ref:
            _, self._doc_ref = self._collection.add(update_dict)
            self.id = self._doc_ref.id
        else:
            ref.update(self._to_update_dict())
        # Mark the fields as saved
        for field in self._fields.values():
            field.mark_saved()

    def __setattr__(self, key, value):
        try:
            fields = super().__getattribute__('_fields')
        except AttributeError:
            # init phase
            return super().__setattr__(key, value)
        else:
            if key in fields:
                return fields[key].set_value(value)
            elif key == 'id' or key == '_doc_ref':
                return super().__setattr__(key, value)
        raise NonExistentFieldException(key)

    def __getattribute__(self, item):
        try:
            return super().__getattribute__(item)
        except AttributeError:
            try:
                fields = super().__getattribute__('_fields')
            except AttributeError:
                pass
            else:
                if item in fields:
                    return fields[item].value

    def __eq__(self, other):
        if other is None:
            return False
        keys = list(self._fields.keys())
        keys.sort()
        other_keys = list(self._fields.keys())
        other_keys.sort()
        if self.id == other.id and keys == other_keys:
            return all(
                map(lambda key: self._fields[key] == other._fields[key], keys))
        return False


class Document(object):

    def __new__(cls, _doc_snapshot=None, **kwargs):
        # Index the fields if not available
        _init_document_class(cls)
        if not cls._fields:
            raise EmptyDocumentException(cls.__name__)

        fields = dict()
        for key, field in cls._fields.items():
            field_kwargs = dict()
            if _doc_snapshot is not None and _doc_snapshot.exists:
                field_kwargs['value'] = _doc_snapshot.get(key)
            fields[key] = field.initialize_field(key, cls.get_collection(),
                                                 **field_kwargs)
        # Initialize
        document = super().__new__(cls)
        document.__init__()
        document_proxy = _DocumentProxy(
            cls, fields, cls.get_collection(), doc_ref=_doc_snapshot)
        document.initialize(document_proxy, **kwargs)
        return document

    def __init__(self, *args, **kwargs):
        """
        To modify kwargs on init, please override #initialize
        """
        self._proxy = None

    def initialize(self, proxy, **kwargs):
        self._proxy = proxy
        for key, value in kwargs.items():
            setattr(self._proxy, key, value)

    def to_ordered_dict(self):
        ordered_dict = OrderedDict()
        value_dict = self._proxy._to_update_dict(force_all=True)
        keys = list(value_dict.keys())
        keys.sort()
        for key in keys:
            ordered_dict[key] = value_dict[key]
        return ordered_dict

    @classproperty
    def objects(cls):
        """
        Returns a QuerySet object which can be used to query this
        collection.

        :return: A QuerySet to apply queries on
        :rtype: google_firestore_orm.query.QuerySet
        """
        return QuerySet.get_manager(cls)

    def delete(self):
        if hasattr(self.__class__, '_references') and self.id:
            # Check if the reference class contain this specific this object
            for reference_class in self.__class__._references:
                for field_name, field in reference_class._fields.items():
                    query = get_reference_query(
                        reference_class, field, field_name, self.__class__,
                        self.id)
                    if query:
                        if safe_next(query.limit(1).stream()):
                            raise ReferenceException(
                                reference_class.get_collection_name())
        self._proxy.delete()

    def save(self):
        self._proxy.save()

    def __setattr__(self, key, value):
        # Set logic for the proxy
        if key == '_proxy':
            try:
                proxy = self._proxy
            except AttributeError:
                return super().__setattr__(key, None)
            else:
                if proxy is None:
                    return super().__setattr__(key, value)
        # Proxy to set the value of a field
        if key in self._proxy._fields:
            setattr(self._proxy, key, value)

    def __getattribute__(self, item):
        try:
            return super().__getattribute__(item)
        except AttributeError as e:
            try:
                proxy = super().__getattribute__('_proxy')
            except AttributeError:
                pass
            else:
                if proxy is not None:
                    return getattr(proxy, item)
            raise e

    @classmethod
    def get_collection_name(cls):
        """
        Retrieves the name of the collection this document should be stored in

        :return: The name of the firestore collection
        :rtype: str
        """
        return class_to_collection_name(cls.__name__)

    @classmethod
    def get_collection(cls):
        """
        Retrieves the collection object which contains documents of this class

        :return: The collection object
        :rtype: google.cloud.firestore_v1.collection.CollectionReference
        """
        collection_name = cls.get_collection_name()
        return get_configured_client().collection(collection_name)

    def __eq__(self, other):
        if other is None:
            return False
        return self._proxy == other._proxy
