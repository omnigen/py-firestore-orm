from google_firestore_orm.exceptions import ValidationException


def _create_field(self, value, from_db=False):
    field_kwargs = dict()
    if from_db:
        field_kwargs['value'] = value

    initialized_field = self._field.sub_field.initialize_field(
        '', self._field.collection, **field_kwargs)
    if not from_db:
        initialized_field.set_value(value)

    return initialized_field


class List(list):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._field = None
        self._field_proxies = []

    def set_parent_field(self, field):
        self._field = field

    def validate(self):
        for i, field in enumerate(self._field_proxies):
            try:
                field.validate()
            except ValidationException as e:
                raise ValidationException(f'index {i}') from e

    def from_db_value(self, value):
        for sub_value in value:
            self._field_proxies.append(_create_field(self, sub_value, True))
            self.append(self._field_proxies[-1].value)

    def get_db_value(self):
        data = []
        for sub_value in self._field_proxies:
            data.append(sub_value.get_db_value())
        return data

    def append(self, value):
        super().append(value)
        self._field_proxies.append(_create_field(self, value))
        self._field._loaded_from_db = False

    def insert(self, index, value):
        super().insert(index, value)
        self._field_proxies.insert(index, _create_field(self, value))
        self._field._loaded_from_db = False

    def extend(self, iterable):
        for value in iterable:
            self.append(value)

    def pop(self, index=None):
        if index is None:
            index = len(self) - 1
        super().pop(index)
        self._field_proxies.pop(index)
        self._field._loaded_from_db = False

    def remove(self, item):
        index = self.index(item)
        self.pop(index)

    def clear(self):
        super().clear()
        self._field_proxies = []
        self._field._loaded_from_db = False

    def reverse(self):
        super().reverse()
        self._field_proxies.reverse()
        self._field._loaded_from_db = False

    def mark_saved(self):
        for field in self._field_proxies:
            field.mark_saved()

    def delete(self):
        for field in self._field_proxies:
            field.delete()

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        if isinstance(key, slice):
            value = map(lambda sub_value: _create_field(self, sub_value),
                        value)
        self._field_proxies[key] = value
        self._field._loaded_from_db = False

    def __delitem__(self, key):
        super().__delitem__(key)
        del self._field_proxies[key]
        self._field._loaded_from_db = False


class Dict(dict):
    """
    TODO: support update
    """
    allowed_types = (str, int, float)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._field = None

    def set_parent_field(self, field):
        self._field = field

    def validate(self):
        print('validated')
        for key, value in self.items():
            if not isinstance(key, self.allowed_types):
                raise ValidationException('Only native types allowed '
                                          '(check keys)')
            elif not isinstance(value, self.allowed_types):
                raise ValidationException('Only native types allowed '
                                          '(check values)')

    def from_db_value(self, value):
        for key, value in value.items():
            self[key] = value

    def get_db_value(self):
        return self

    def clear(self):
        super().clear()
        self._field._loaded_from_db = False

    def pop(self, key, default=None):
        super().pop(key, default)
        self._field._loaded_from_db = False

    def popitem(self):
        self._field._loaded_from_db = False
        return super().popitem()

    def setdefault(self, key, default=None):
        self._field._loaded_from_db = False
        return super().setdefault(key)

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        self._field._loaded_from_db = False

    def __delitem__(self, key):
        super().__delitem__(key)
        self._field._loaded_from_db = False
